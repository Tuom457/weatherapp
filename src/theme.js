import { createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    background: {
      default: '#f8f9fa',
      paper: '#ffffff',
    },
    primary: {
      main: '#000',
      light: '#e5f6fd',
    },
    secondary: {
      main: '#000',
      light: '#e6e6e6',
    },
  },
  typography: {
    h1: {
      fontSize: 40,
      color: '#262626',
    },
    h2: {
      color: '#262626',
      fontSize: 30,
    },
    body1: {
      color: '#262626',
      fontSize: 20,
    },
    body2: {
      fontSize: 15,
      color: '#70757A',
    },
    fontFamily: 'Arial',
  },
});

export default theme;

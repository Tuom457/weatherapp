import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const SubWeatherCard = ({ weatherData, getPrecipitation, convertTemp }) => {
  const classes = useStyles();

  return (
    <article className={classes.root}>
      <Typography variant="body2">
        {new Date(weatherData.dt_txt).getHours()}:00
      </Typography>
      <div>
        <img
          className={classes.icon}
          src={`http://openweathermap.org/img/wn/${weatherData.weather[0].icon}.png`}
        />
      </div>
      <Typography variant="body1">
        {convertTemp(weatherData.main.temp)}°C
      </Typography>
      <div className={classes.secondaryData}>
        <Typography variant="body2">
          {weatherData.wind.speed}
          <span> m/s</span>
        </Typography>
        <Typography variant="body2">
          {weatherData.main.humidity}
          <span>%</span>{' '}
        </Typography>
        <Typography variant="body2">
          {getPrecipitation(weatherData.rain || weatherData.snow)}{' '}
          <span>mm</span>{' '}
        </Typography>
      </div>
    </article>
  );
};

export default SubWeatherCard;

const useStyles = makeStyles(({ palette }) => {
  return {
    root: {
      minWidth: '50pt',
      maxWidth: '100pt',
      marginTop: '15pt',
      textAlign: 'center',
      paddingTop: '5pt',
      border: `1pt solid ${palette.secondary.light}`,
      borderRadius: '5pt',
      backgroundColor: palette.background.paper,
    },
    icon: {
      width: '30pt',
      height: '30pt',
    },
    secondaryData: {
      textAlign: 'center',
      backgroundColor: palette.primary.light,
      padding: '5pt',
      marginTop: '5pt',
      '@media (max-width: 400px)': {
        '& span': {
          display: 'block',
          fontSize: '7pt',
        },
      },
    },
  };
});

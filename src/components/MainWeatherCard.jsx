import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const MainWeatherCard = ({
  weatherData,
  getPrecipitation,
  convertTemp,
  cityName,
}) => {
  const classes = useStyles();

  const getEndingForDate = (day) => {
    // get ending for dates used in the code; 1st, 2nd, 3rd, 4th...
    if (day > 3 && day < 21) return 'th';
    switch (day % 10) {
      case 1:
        return 'st';
      case 2:
        return 'nd';
      case 3:
        return 'rd';
      default:
        return 'th';
    }
  };

  const date = new Date();

  return (
    <article className={classes.root}>
      <div className={classes.content}>
        <div>
          <Typography variant="h2">{cityName}</Typography>
          <Typography variant="body2">
            {weatherData.weather[0].description}
          </Typography>
        </div>
        <div className={classes.tempData}>
          <img
            className={classes.icon}
            src={`http://openweathermap.org/img/wn/${weatherData.weather[0].icon}.png`}
          />
          <Typography variant="h2">
            {convertTemp(weatherData.main.temp)} °C
          </Typography>
        </div>
      </div>
      <div className={classes.content}>
        <div className={classes.timeData}>
          <Typography variant="body1">
            {date.toLocaleString('en-EN', { month: 'short', day: 'numeric' })}
            {getEndingForDate(date.getDate)}
          </Typography>

          <Typography variant="body2">
            {date.getHours()}:
            {date.getMinutes() < 10 ? '0' : '' + date.getMinutes()}
          </Typography>
        </div>
        <div className={classes.secondaryData}>
          <Typography variant="body2">
            Wind: {weatherData.wind.speed} m/s
          </Typography>

          <Typography variant="body2">
            Humidity: {weatherData.main.humidity} %
          </Typography>

          <Typography variant="body2">
            Prepicitation (3h):{' '}
            {getPrecipitation(weatherData.rain || weatherData.snow)} mm
          </Typography>
        </div>
      </div>
    </article>
  );
};

export default MainWeatherCard;

const useStyles = makeStyles(({ palette }) => {
  return {
    root: {
      padding: '10pt',
      marginTop: '15pt',
      border: `1pt solid ${palette.secondary.light}`,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-between',
      borderRadius: '5pt',
      backgroundColor: palette.background.default,
      height: '125pt',
    },
    content: {
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    icon: {
      width: '50pt',
      height: '50pt',
      marginTop: '-10pt',
    },
    tempData: {
      display: 'flex',
    },
    secondaryData: {
      textAlign: 'right',
    },
    timeData: {
      alignSelf: 'flex-end',
    },
  };
});

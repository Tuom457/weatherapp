import React, { useState, useEffect } from 'react';
import WeatherForecastContainer from './components/WeatherForecastContainer';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import {
  getCurrentWeatherData,
  getThreeHourWeatherData,
} from './services/weatherApiservice';

import config from './config';
import { Typography } from '@material-ui/core';

function App() {
  const { apiKey, cities } = config;
  const [weatherData, setWeatherData] = useState(null);
  const [error, setError] = useState(null);

  const classes = useStyles();

  useEffect(() => {
    (async () => {
      try {
        const weatherData = await Promise.all(
          cities
            .sort((a, b) => a.cityName.localeCompare(b.cityName))
            .map(async (city) => {
              const currentWeatherData = await getCurrentWeatherData(
                city,
                apiKey,
              );
              const threeHourWeatherData = await getThreeHourWeatherData(
                city,
                apiKey,
              );
              return {
                cityName: city.cityName,
                cityId: city.cityId,
                currentWeatherData,
                threeHourWeatherData,
              };
            }),
        );
        setWeatherData(weatherData);
      } catch (error) {
        setError('Error fetching data');
      }
    })();
  }, []);

  return (
    <main className={classes.app}>
      <header className={classes.headerBar}>
        <Typography variant="h1" className={classes.appHeader}>
          Weather forecast
        </Typography>
      </header>
      {error ? (
        <div className={classes.error}>
          <Typography variant="body1">{error}</Typography>
        </div>
      ) : (
        <div>
          {!error && weatherData ? (
            <WeatherForecastContainer
              allWeatherData={weatherData}
              cities={cities}
            />
          ) : (
            <div className={classes.spinner}>
              <CircularProgress size="50pt" />
            </div>
          )}
        </div>
      )}
    </main>
  );
}

export default App;

const useStyles = makeStyles(({ palette }) => {
  return {
    app: {
      maxWidth: '400pt',
      margin: 'auto',
      backgroundColor: palette.background.default,
      marginBottom: '30pt',
    },
    headerBar: {
      backgroundColor: palette.background.paper,
      height: '50pt',
      borderBottom: `1pt solid ${palette.secondary.light}`,
      textAlign: 'center',
    },
    appHeader: {
      position: 'relative',
      top: 15,
    },
    error: {
      textAlign: 'center',
      padding: '50pt',
    },
    spinner: {
      textAlign: 'center',
      padding: '50pt',
    },
  };
});

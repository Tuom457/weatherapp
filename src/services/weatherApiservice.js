import config from '../config';

export const getCurrentWeatherData = async (city, apiKey) => {
  const currentWeatherRes = await fetch(
    `${config.backendBaseUrl}/weather?id=${city.cityId}&appid=${apiKey}`,
  );
  if (currentWeatherRes.status !== 200) {
    return Promise.reject();
  }
  return await currentWeatherRes.json();
};

export const getThreeHourWeatherData = async (city, apiKey) => {
  const threeHourWeatherRes = await fetch(
    `${config.backendBaseUrl}/forecast?id=${city.cityId}&appid=${apiKey}`,
  );
  if (threeHourWeatherRes.status !== 200) {
    return Promise.reject();
  }
  return await threeHourWeatherRes.json();
};

import * as React from 'react';
import { render } from '@testing-library/react';
import { expect } from 'chai';
import App from './App';
import { getCurrentWeatherData } from './services/weatherApiservice';
import config from './config';

describe('<App>', () => {
  it('renders app header', () => {
    const { getByText } = render(<App />);
    const Header = getByText(/Weather forecast/i);
    expect(document.body.contains(Header));
  });
});

describe('Test weather api', () => {
  it('gets needed weatherdata', async () => {
    const data = await getCurrentWeatherData(
      config.cities.find((city) => city.cityName === 'Tampere'),
      config.apiKey,
    );
    expect(data).to.haveOwnProperty('weather');
    const generalWeather = data.weather[0];
    expect(generalWeather).to.haveOwnProperty('main');
    expect(generalWeather).to.haveOwnProperty('description');
    expect(generalWeather).to.haveOwnProperty('icon');
  });
});
